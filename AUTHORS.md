# Authors

Currently maintained by the **Kadi4Mat Team**.

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Nicolas Christ**
* **Nico Brandt**
* **Ephraim Schoof**
* **Patrick Altschuh**
* **Deepalaxmi Rajagopal**
* **Arnd Koeppe**
* **Matthieu Laqua**
* **Manideep Jayavarapu**
* **Julian Grolig**

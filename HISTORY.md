# Release history

## 0.26.0 (2023-01-09)

* Added a `file_description` parameter for file upload functions in both
  library and CLI.
* Improved the `upload_file` method for uploading smaller files without the
  need for chunking.
* Added the `ro-crate` export type to the corresponding CLI commands of records
  and collections.

## 0.25.0 (2022-10-14)

* Improved the `get_metadatum` methods for records to retrieve nested metadata,
  which also now works in the corresponding CLI command.
* The record library version of `get_metadatum` does not use the `information`
  parameter anymore and returns `None` in case the requested metadatum was not
  found.
* Added functionality to retrieve templates shared with a group via the
  library.
* Added functionality to manage user and group roles of templates via the
  library.
* Added functionality to retrieve group roles of all resources via the library.

## 0.24.0 (2022-08-22)

* Fixed retrieval of deleted resources via the CLI.
* Fixed usage of some CLI related classes outside of builtin commands.
* Fixed purging of some resource types via the CLI.
* Deleted templates can now be purged and restored via the CLI.
* Added functionality to export templates.
* All export functions and commands now default to the JSON export type.

## 0.23.0 (2022-04-27)

* Added a `license` option to the `records edit` CLI subcommand.
* Removed support for Python 3.6, as it has reached end of life.

## 0.22.0 (2022-03-07)

* The unit does not have to be specified in the dict using the `add_metadatum`
  function within the CLI/lib.

## 0.21.0 (2022-01-17)

* Removed the `click_completion` dependency. Autocompletion is now only
  supported for bash, fish and zsh.
* Fixed the export function via using the correct parameter.
* Added handling of the `ConnectionError`.
* Allow the `search_pagination_options` decorator to set an user-specific
  description for the option `page`.
* Refactoring of the record linkage, as the check for identical links is
  already carried out in Kadi.
* Refactoring of the export function. The file is now already stored using the
  export function defined in the lib part.
* Add an example if metadata have not the right input format when using the
  `add_metadata` (CLI) function.

## 0.20.0 (2021-11-26)

* Added support for new collaborator role.
* Added exclude options and a query parameter in the `get-records` and
  `get-collections` CLI commands.
* Added utility function to create identifier from arbitrary string input.

## 0.19.0 (2021-11-24)

* Increased the upload speed of small files.
* Add option to pipe a list of downloaded files as tokenlist.
* Relaxed required Python version again.
* Add functions to use the `GET /api/tags` endpoint.

## 0.18.0 (2021-11-05)

* Include functions to export a collection.
* Changed default separator in `search_resource_ids` to `,`.
* Add option so search records in child collections.
* Include option to work with tokenlists.

## 0.17.1 (2021-11-04)

* Added feature to export a record to a folder based on the identifier.

## 0.17.0 (2021-11-04)

* Include methods using the export endpoint.
* Improve usability of config add-instance.
* Use of more mixin classes in the CLI lib.
* Add AUTHORS.md.
* Add the GET /api/roles endpoint.
* Add CLI function to edit basic metadata of a template.
* Replaced hide-public with visibility parameter.
* Refactoring the use of raise_request_error().
* Renamed all `replace` keywords to `force`.
* Added a `get_metadatum` method to the lib record.
* Added functions to update a record link (name).
* Added options to use a tokenlist in the search.
* Take into account corner case if only a scope to create a resource is given.
* Make get_metadatum work with nested types.

## 0.16.0 (2021-07-14)

* Include search function which returns a list of ids.
* Include CLI tools to search for record and collection ids.

## 0.15.0 (2021-06-24)

* Add factory methods for base functions into CLIKadiManager.
* Do not validate metadatum if value is `None`.

## 0.14.0 (2021-06-17)

* Improve documentation.

## 0.13.0 (2021-06-08)

* Add option to store record files in a folder based on the record's
  identifier.
* Add basic validation for host input.
* Add more CLI config tools.

## 0.12.0 (2021-06-01)

* Doc improvements.
* Add option to use the KadiManager via the '@apy_command' decorator.

## 0.11.1 (2021-05-18)

* Smaller refactoring in docs.
* Fix bash example script.
* Show also username in user search.
* Set identity type to required in case of specifying user via username.

## 0.11.0 (2021-05-11)

* Include docs.
* Refactor verbose.
* Introduce a `CLIKadiManager`.

## 0.10.0 (2021-04-29)

* Update `README.md`.
* Changed two required arguments of `upload_file` method of `CLIRecord` class
  to optional.
* Include verbose level to manage the amount of print outputs.

## 0.9.1 (2021-04-23)

* Add pypi deploy runner.

## 0.9.0 (2021-04-22)

* Add CLI tools to configure the config file.
* Move `get-kadi-info` from CLI group `miscellaneous` to `config`.

## 0.8.0 (2021-04-12)

* Add default value in `records get-metadatum`.
* Add `get_file_info` function for records.
* Return list of downloaded file(s) in `get_file`
* Add experimental CLI tool to download and execute a workflow.
* Add CLI tool `kadi-apy miscellaneous get-kadi-info`.
* Exit with error code 1 during uploading a file in case the file already
  exists and should not be replaced.

## 0.7.0 (2021-03-24)

* Add `exit-not-created` flag to CLI create tools.
* Fix order in which `_meta` is invalidated.
* Add option to download only those files from record matching pattern.
* Add char options for decorator to define options of files and resources.
* Add CLI tool `add-string-as-file`.

## 0.6.0 (2021-02-26)

* Add option to use certificates from ca_bundle.
* Add option skip the initial request.

## 0.5.0 (2021-02-24)

* Include a config file to store the information about host and PAT.
* Added timeout config option.
* Add autocompletion.

## 0.4.1 (2021-02-02)

* Include the --version output.
* Add handling for missing schema exception.
* Unify the behavior of decorators if information is not required.
* Add --xmlhelp runner

## 0.4.0 (2021-01-22)

* Add option to hide public resources in the CLI search.
* Functions to handle miscellaneous (license search, remove item from trash).
* Add function to remove all metadata from a record.

## 0.3.3 (2021-01-14)

* Smaller cleanups.

## 0.3.2 (2021-01-14)

* Include missing `__init__.py`.

## 0.3.1 (2021-01-14)

* Include xmlhelpy import from pypi.

## 0.3.0 (2021-01-14)

* Include definition of public api.
* Integration of --xmlhelp option to all CLI tools.
* Definition of CLI classes which can be used in additions tools.
* Print more infos when using the CLI.
* Various refactoring.

## 0.2.3 (2020-10-23)

* Fix for specifying PAT and host directly in certain cli commands.

## 0.2.2 (2020-10-23)

* Raise exception instead of sys.exit(1) for cli functions.

## 0.2.1 (2020-10-05)

* Update examples.

## 0.2.0 (2020-10-02)

* Work with identifiers for items.

## 0.1.2 (2020-09-25)

* Include group roles for records and collections.
* Add option to skip ssl/tls cert verification.

## 0.1.1 (2020-09-21)

* Improved printing for updated metadata.
* Smaller improvements.

## 0.1.0 (2020-09-09)

* Most API endpoints for managing records, collections and groups, as well as
  some others, are usable directly through the library.
* Most of that functionality is also exposed via CLI by using the kadi-apy
  command.

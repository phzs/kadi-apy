Library
=======

The Python library is the most important and most flexible way to use the functionality
that kadi-apy provides. The following shows a basic example on how to use it:

.. code-block:: python3

    from kadi_apy import KadiManager

    manager = KadiManager()

    # Create the new record with the given identifier if it does not exist yet.
    record = manager.record(identifier="my_record", create=True)
    record.upload_file("/path/to/file")

.. note::
    Most methods directly return a response object as defined in the `requests
    <https://requests.readthedocs.io/en/latest>`__ Python library.

.. include:: ../snippets/kadi_docs.rst

KadiManager
-----------

.. autoclass:: kadi_apy.lib.core.KadiManager
    :members:

Record
------

.. autoclass:: kadi_apy.lib.resources.records.Record
    :members:
    :inherited-members:

Collection
----------

.. autoclass:: kadi_apy.lib.resources.collections.Collection
    :members:
    :inherited-members:

Template
--------

.. autoclass:: kadi_apy.lib.resources.templates.Template
    :members:
    :inherited-members:

User
----

.. autoclass:: kadi_apy.lib.resources.users.User
    :members:
    :inherited-members:

Group
-----

.. autoclass:: kadi_apy.lib.resources.groups.Group
    :members:
    :inherited-members:

Miscellaneous
-------------

.. autoclass:: kadi_apy.lib.miscellaneous.Miscellaneous
    :members:
    :inherited-members:

SearchResource
--------------

.. autoclass:: kadi_apy.lib.search.SearchResource
    :members:
    :inherited-members:

SearchUser
----------

.. autoclass:: kadi_apy.lib.search.SearchUser
    :members:
    :inherited-members:

Exceptions
----------

.. automodule:: kadi_apy.lib.exceptions
    :members:
    :show-inheritance:

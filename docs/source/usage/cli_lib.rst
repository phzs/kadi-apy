CLI Library
===========

The Python CLI library is mainly useful to implement custom CLI commands. The provided
functionality is usually less flexible in comparison with the regular Python library,
but it also provides some built-in error handling and console output.

In general, its functionality can be used similarly to the regular Python library:

.. code-block:: python3

    from kadi_apy import CLIKadiManager

    manager = CLIKadiManager()

    record = manager.record(identifier="my_record", create=True)
    record.upload_file("/path/to/file")

.. include:: ../snippets/kadi_docs.rst

CLIKadiManager
--------------

.. autoclass:: kadi_apy.cli.core.CLIKadiManager
    :members:
    :show-inheritance:
    :inherited-members:

CLIRecord
---------

.. autoclass:: kadi_apy.cli.lib.records.CLIRecord
    :members:
    :inherited-members:
    :show-inheritance:

CLICollection
-------------

.. autoclass:: kadi_apy.cli.lib.collections.CLICollection
    :members:
    :inherited-members:
    :show-inheritance:

CLITemplate
-----------

.. autoclass:: kadi_apy.cli.lib.templates.CLITemplate
    :members:
    :inherited-members:
    :show-inheritance:

CLIUser
-------

.. autoclass:: kadi_apy.cli.lib.users.CLIUser
    :members:
    :inherited-members:
    :show-inheritance:

CLIGroup
--------

.. autoclass:: kadi_apy.cli.lib.groups.CLIGroup
    :members:
    :inherited-members:
    :show-inheritance:

CLIMiscellaneous
----------------

.. autoclass:: kadi_apy.cli.lib.miscellaneous.CLIMiscellaneous
    :members:
    :inherited-members:

CLISearchResource
-----------------

.. autoclass:: kadi_apy.cli.search.CLISearchResource
    :members:
    :inherited-members:
    :show-inheritance:

CLISearchUser
-------------

.. autoclass:: kadi_apy.cli.search.CLISearchUser
    :members:
    :inherited-members:
    :show-inheritance:

Decorators
----------

.. automodule:: kadi_apy.cli.decorators
    :members:
    :inherited-members:
    :show-inheritance:

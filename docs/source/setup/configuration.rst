Configuration
=============

To connect an instance of Kadi4Mat to a local installation of this library, the host of
the Kadi4Mat instance as well as a personal access token (PAT) (or just token) are
required.

The host is the fully qualified domain name of the Kadi4Mat instance, e.g.
``https://demo-kadi4mat.iam-cms.kit.edu`` or ``http://localhost:5000`` for a typical
local development installation.

A PAT can be created via the web interface of a Kadi4Mat instance in the menu found in
*Settings > Access tokens* and will operate with the same permissions as the user that
created the token. Besides specifying the name of the token, which only becomes
important when managing multiple tokens, an expiration date can be set. This date can
optionally be used to prohibit the usage of the token beyond the specified date. If no
date is given, the token will never expire. As another security feature, a token can be
limited to one or multiple *scopes*. Scopes can restrict a token's access to specific
resources or actions. If no scopes are selected, the token will have full access by
default.

You can either directly pass the token and host to the ``KadiManager`` or
``CLIKadiManager``, e.g.

.. code-block:: python3

    from kadi_apy import KadiManager

    manager = KadiManager(host="your_host", token="your_token")

or use a config file to store those information. If no information of the host and token
are given to the ``KadiManager``, the ``KadiManager`` tries to load these values from
the config file. The kadi-apy command line interface provides functions to configure the
config file. You can run

.. code-block:: shell

    kadi-apy config create

which creates the file ``.kadiconfig`` in you home directory. Use

.. code-block:: shell

    kadi-apy config set-host

and

.. code-block:: shell

    kadi-apy config set-pat

to store the corresponding values or open the file to fill in the host and PAT
information. E.g.:

.. code-block:: shell

    [global]                        # Global settings.
    timeout = 60                    # Timeout in seconds for the requests.
    default = my_instance           # The default Kadi4Mat instance to use.
    verify = True                   # Whether to verify the SSL/TLS certificate of the host.
    ca_bundle = /path/to/certfile   # Optional CA bundle for SSL/TLS verification.

    [my_instance]
    host = https://example.com/
    pat = <your PAT>

    [my_second_instance]
    host = https://example2.com/
    pat = <your second PAT>

When using the CLI, you can choose the instance to use. The default is defined by the
key ``default`` in the section ``[global]``. Optionally, you can include a path to
certificates of trusted CAs in the config file via adding ``ca_bundle =
/path/to/certfile`` which is used in case of ``verify = True``.

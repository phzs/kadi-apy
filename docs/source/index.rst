Welcome to kadi-apy's documentation!
====================================

|pypi| |license| |zenodo|

.. |pypi| image:: https://img.shields.io/pypi/v/kadi-apy
    :target: https://pypi.org/project/kadi-apy/
    :alt: PyPi

.. |license| image:: https://img.shields.io/pypi/l/kadi-apy
    :target: https://opensource.org/licenses/Apache-2.0
    :alt: License

.. |zenodo| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.4088275.svg
    :target: https://doi.org/10.5281/zenodo.4088275
    :alt: Zenodo

**Kadi-APY** is a library for use in tandem with `Kadi4Mat
<https://gitlab.com/iam-cms/kadi>`__. The REST-like API of Kadi4Mat makes it possible to
programmatically interact with most of the resources that can be used through the web
interface by sending suitable HTTP requests to the different endpoints the API provides.

The goal of this library is to make the use of this API as easy as possible. It offers
both an object oriented approach to work with the API in Python as well as a command
line interface. The library is written in Python 3 and works under both Linux and
Windows. The source code of the project can be found `here
<https://gitlab.com/iam-cms/kadi-apy>`__.

.. include:: snippets/kadi_docs.rst

----

.. toctree::
    :name: setup
    :caption: Setup
    :maxdepth: 1

    setup/installation
    setup/development
    setup/upgrading
    setup/configuration

This chapter describes how to install, upgrade and configure kadi-apy.

----

.. toctree::
    :name: usage
    :caption: Usage
    :maxdepth: 1

    usage/lib
    usage/cli
    usage/cli_lib

The library can be used by via Python or via a command line interface (CLI).
Additionally, various functionality exists to implement custom CLI commands (CLI
Library). Please also see the different `example files
<https://gitlab.com/iam-cms/kadi-apy/-/blob/master/examples>`__ in the code repository.

----

.. toctree::
    :name: release-history
    :caption: Release history
    :maxdepth: 2

    HISTORY.md
